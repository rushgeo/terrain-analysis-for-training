## Terrain Analysis

To receive a DEM as input from the user and apply Evans-Young methods to calculate morphometric parameters in parallel. 
The value for cells with no data and the index of the desired band to execute can be specified upon the start of the program. 
Returned output is in equivalent format as that of the input. 

##### Running the program:

mpirun -np <procs> python terrain_main.py input=<file> output=<file> [--nodata=<value>] [--band=<value>] [--verbose] [--debug]

#### For further information:
[Go to the CIGI Wiki](https://wiki.cigi.illinois.edu/display/UP/Parallel+Terrain+Analysis+on+DEMs)